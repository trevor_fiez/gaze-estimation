/*
 * Written by Trevor Fiez
 * 2014
 * fieztr@onid.oregonstate.edu
 */

#include "test_face.h"

/** Global variables */
String face_cascade_name = "/home/trevor/opencv-2.4.9/data/haarcascades/haarcascade_frontalface_alt.xml";
String eyes_cascade_name = "/home/trevor/opencv-2.4.9/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);

/** @function main */

int main( int argc, char *argv[] )
{
	if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };
	if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };

	VideoCapture vid("./videos/lydia/lydia.MOV");
	string output_vid_name = "./videos/lydia/lydia_ref.avi";
	int vid_height = (int) vid.get(CV_CAP_PROP_FRAME_HEIGHT);
	int vid_width = (int) vid.get(CV_CAP_PROP_FRAME_WIDTH);
	
	VideoWriter out_vid(output_vid_name, CV_FOURCC('D', 'I', 'V', 'X'), 30, Size(vid_width, vid_height));
	

	Mat face_frame, orig_face;
	

	vid >> face_frame;
	orig_face =  face_frame.clone();
	
	double frame_num = vid.get(CV_CAP_PROP_FRAME_COUNT);
	

	vector<Mat> eyes;
	vector<Rect> faces;
	vector<vector<Rect> > eye_rects;
	vector<Point2i> eye_index;
	

	eyes = detectAndDisplay(face_frame, "george", eye_index, faces, eye_rects);
	
	cout << "before: " << eyes.size() << " " << faces.size() << endl;
	Point2i im_size((int) face_frame.cols, (int) face_frame.rows);
	face_eye_detection_filtering(eye_index, eyes, faces, im_size, eye_rects);
	
	
	cout << "after: " << eyes.size() << " " << faces.size() << endl;

	
	left_right_eyes(eyes, eye_index, eye_rects);
	
	
	eye_rects[0][0].x = eye_index[0].x;
	eye_rects[0][0].y = eye_index[0].y;
	eye_rects[0][1].x = eye_index[1].x;
	eye_rects[0][1].y = eye_index[1].y;
	
	Mat gray_frame;
	cvtColor(orig_face, gray_frame, CV_BGR2GRAY);
	Mat new_left(gray_frame, eye_rects[0][0]);
	Mat new_right(gray_frame, eye_rects[0][1]);
	eyes[0] = new_left;
	eyes[1] = new_right;

	
	eyes_data sultan_eyes("./videos/out_no_color_sultan", eyes, faces[0], eye_rects[0], eye_index);

	string quilt_vid_name = "./videos/lydia/blink_ref_quilt.avi";
	

	VideoWriter quilt_out(quilt_vid_name, CV_FOURCC('D', 'I', 'V', 'X'), 30, Size(eyes[1].cols * 2, eyes[1].rows * 2));

	cout << "Blurring" << endl;
		
	sultan_eyes.median_blur_eyes(5);
	
	sultan_eyes.set_iris_size();

	
	sultan_eyes.all_scores_highest();
	
	sultan_eyes.ref_pt.push_back(sultan_eyes.pupils[0]);
	sultan_eyes.ref_pt.push_back(sultan_eyes.pupils[1]);
	
	/*
	string iris_name = "./videos/trevor/right_iris.avi";
	
	VideoWriter iris_vid(iris_name, CV_FOURCC('D', 'I', 'V', 'X'), 30, eyes[1].size(), false);

	double iris_max = 0;
	int max_score_index = -1;
	for (int i = 0; i < sultan_eyes.iris_mats[1].size(); i++) {

		
		if (sultan_eyes.iris_max_score[1][i] > iris_max) {
			iris_max = sultan_eyes.iris_max_score[1][i];
			max_score_index = i;
		}
	}
	
	for (int i = 0; i < sultan_eyes.iris_mats[1].size(); i++) {
		Mat adjusted = norm_char_set_max(sultan_eyes.iris_mats[1][i], iris_max);
		cout << sultan_eyes.iris_max_score[1][i] << endl;

		if(i == max_score_index) {
			
			imshow("Pupil scores", adjusted);
			cout << "max score " << i << endl;
			
			waitKey(0);
		}
		iris_vid << norm_char_set_max(sultan_eyes.iris_mats[1][i], iris_max);
	}
	
	return 1;
	*/

	

	cout << "Drawing Face" << endl;
		
	sultan_eyes.draw_face();
	
	cout << "OUTPUTING Vid" << endl;
	
	out_vid << sultan_eyes.cur_frame;

	
	vid >> face_frame;
	
	cout << "UPdating frame" << endl;
	
	sultan_eyes.update_cur_frame(face_frame);
	
	int64 start_ticks = getTickCount();
	
	for (double i = 1; i < frame_num - 1; i++) {
	
		cout << "Blurring" << endl;
		
		sultan_eyes.median_blur_eyes(5);
		
		
		cout << "Wrapper" << endl;
		//sultan_eyes.local_wrapper_ref_thresh();
		
		sultan_eyes.local_wrapper();
		sultan_eyes.kmeans_velocity_filter(5);
		
		

		//sultan_eyes.wrapper();

/*	
		sultan_eyes.search_features_ref_thresh();
		sultan_eyes.search_features();
 
		sultan_eyes.compute_both_searches();

		Mat q = sultan_eyes.create_eye_quilt(1);
				
		quilt_out  << q;
*/	

		
	
		
		
		sultan_eyes.draw_face();
		
		cout << "OUTPUTING Vid" << endl;
		
		out_vid << sultan_eyes.cur_frame;

		vid >> face_frame;
		
		cout << "UPdating frame" << endl;
		
		sultan_eyes.update_cur_frame(face_frame);

		cout << "finished frame" << endl;
		
		

		
	}
	
	int64 end_ticks = getTickCount();
	
	int64 total_ticks = end_ticks - start_ticks;
	
	double total_time = ((double) total_ticks) / getTickFrequency();
	
	double average_time = total_time / frame_num;
	
	cout << "The total time to run through the video was: " << total_time << " for 225 frames" << endl;
	cout << "The average time per frame is: " << average_time << endl;
		
	return 0;
}

eyes_data::eyes_data (string new_name) {
	name = new_name;
}

eyes_data::eyes_data(string new_name, vector<Mat> eyes, Rect face_rect, vector<Rect> eye_rectangle, vector<Point2i> eye_offsets)
{
	name = new_name;
	cur_eyes = eyes;
	face = face_rect;
	eye_rects = eye_rectangle;
	eye_index = eye_offsets;
	
	create_distance_mat();
	
	l_max_gx = 0;
	r_max_gx = 0;
	threshold_const = 0.65;
	largest_blinks = 0;
	consec_blinks = 0;
}

void eyes_data::median_blur_eyes(int size)
{
	for (int i = 0; i < cur_eyes.size(); i++)
		medianBlur(cur_eyes[i], cur_eyes[i], size);
	
}

void eyes_data::update_cur_frame(Mat new_frame)
{
	cur_frame = new_frame;
	
	Mat gray_frame;
	cvtColor(cur_frame, gray_frame, CV_BGR2GRAY );
	
	
	
	Mat new_left(gray_frame, eye_rects[0]);
	Mat new_right(gray_frame, eye_rects[1]);
	
	
	cur_eyes[0] = new_left;
	cur_eyes[1] = new_right;
	
	
	if (prev_pupils.size() > 0)
		prev_pupils.clear();
	if (prev_loc_scores.size() > 0)
		prev_loc_scores.clear();
	
	for (int i = 0; i < 2; i++) {
		prev_pupils.push_back(pupils[i]);
		prev_loc_scores.push_back(loc_scores[i]);
	}
	
	prev_p_detections.push_back(pupils);
	
	if (cur_gx.size() > 0)
		cur_gx.clear();
	if (cur_gy.size() > 0)
		cur_gy.clear();
	if (cur_mag.size() > 0)
		cur_mag.clear();
	if (left_pts.size() > 0)
		left_pts.clear();
	if (right_pts.size() > 0)
		right_pts.clear();
	if (cur_scores.size() > 0)
		cur_scores.clear();
	if (pupils.size() > 0)
		pupils.clear();
	if (loc_scores.size() > 0)
		loc_scores.clear();
	if (cur_local_scores.size() > 0)
		cur_local_scores.clear();
	if (abs_pupils.size() > 0)
		abs_pupils.clear();
	
	cout << "Prev pupils scores " << prev_pupils.size() << endl;
	cout << "prev loc scores " << prev_loc_scores.size() << endl;
	
}

void eyes_data::average_loc_filter(int look_size)
{
	
	Point2i avg_left(0, 0);
	Point2i avg_right(0, 0);
	
	int frame_num = prev_p_detections.size();
	if (look_size > frame_num)
		look_size = frame_num;
	
	for (int i = 1; i <= look_size; i++) {
		avg_left.x += prev_p_detections[frame_num - i][0].x;
		avg_left.y += prev_p_detections[frame_num - i][0].y;
		avg_right.x += prev_p_detections[frame_num - i][1].x;
		avg_right.y += prev_p_detections[frame_num - i][1].y;
	}
	
	avg_left.x = avg_left.x / look_size;
	avg_left.y = avg_left.y / look_size;
	avg_right.x = avg_right.x / look_size;
	avg_right.y = avg_right.y / look_size;
	
	vector<Point2i> show_vecs;
	show_vecs.push_back(avg_left);
	show_vecs.push_back(avg_right);
	
	pt_shown.push_back(show_vecs);
}

void eyes_data::kmeans_velocity_filter(int look_size)
{
	
	if (prev_p_detections.size() < 8) {
		vector<Point2i> show_vecs;
		show_vecs.push_back(pupils[0]);
		show_vecs.push_back(pupils[1]);
		pt_shown.push_back(show_vecs);
		return;
	}
	Point2d temp;
	double v;
	int i = 0;
	int frame_num = prev_p_detections.size() - 1;
	vector<Point2i> show_vecs;
	for (int eye = 0; eye < 2; eye++) {
		temp = Point2d((double) pupils[eye].x - prev_p_detections[frame_num - i][eye].x,
					(double) pupils[eye].y - prev_p_detections[frame_num - i][eye].y);
	
		v = sqrt((temp.x * temp.x) + (temp.y * temp.y));
		if (v > 10)
			show_vecs.push_back(pt_shown.back()[eye]);
		else
			show_vecs.push_back(pupils[eye]);
	}
	
	pt_shown.push_back(show_vecs);
	return;

	
	/*int frame_num = prev_p_detections.size() - 1;
	
	if (look_size - 1 > frame_num) {
		vector<Point2i> show_vecs;
		show_vecs.push_back(pupils[0]);
		show_vecs.push_back(pupils[1]);
		pt_shown.push_back(show_vecs);
		return;
	}
	
	vector<double> l_v, r_v;
	Point2d temp;
	Point2d a, b;
	double v;
	for (int eye = 0; eye < 2; eye++) {
		for (int i = 1; i < 4; i++) {
			if (i == 0) {
				temp = Point2d((double) pupils[eye].x - prev_pupils[frame_name - i][eye].x,
					(double) pupils[eye].y - prev_pupils[frame_name - i][eye].y);
				
			} else {
				a = prev_pupils[frame_name - i][eye];
				b = prev_pupils[frame_name - i - 1][eye]
				temp = Point2d((double) a.x - b.x, (double) a.y - b.y);
			}
			
			v = sqrt((temp.x * temp.x) + (temp.y * temp.y));
			if (eye)
				r_v.push_back(v);
			else
				l_v.push_back(v);
		}
	}*/
}


void eyes_data::kmeans_filter(int look_size)
{
	int frame_num = prev_p_detections.size() + 1;
	if (look_size > frame_num) {
		vector<Point2i> show_vecs;
		show_vecs.push_back(pupils[0]);
		show_vecs.push_back(pupils[1]);
		pt_shown.push_back(show_vecs);
		return;
	}
	

	Mat left_samples(look_size, 2, CV_32F);
	Mat right_samples(look_size, 2, CV_32F);
	
	left_samples.at<float>(0, 0) = (float) pupils[0].x;
	left_samples.at<float>(0, 1) = (float) pupils[0].y;
	right_samples.at<float>(0, 0) = (float) pupils[1].x;
	right_samples.at<float>(0, 1) = (float) pupils[1].y;
	

	for (int i = 1; i < look_size; i++) {
		left_samples.at<float>(i, 0) = (float) prev_p_detections[frame_num - i - 1][0].x;
		left_samples.at<float>(i, 1) = (float) prev_p_detections[frame_num - i - 1][0].y;
		right_samples.at<float>(i, 0) = (float) prev_p_detections[frame_num - i - 1][1].x;
		right_samples.at<float>(i, 1) = (float) prev_p_detections[frame_num - i -1 ][1].y;
	}
	

	
	Mat l_labels, r_labels;
	Mat l_centers, r_centers;
	

	kmeans(left_samples, look_size, l_labels, TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 500, 0.0001), 2, KMEANS_PP_CENTERS, l_centers);
	kmeans(right_samples, look_size, r_labels, TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 500, 0.0001), 2, KMEANS_PP_CENTERS, r_centers);
	
	int clust_zero = 0;
	int clust_one = 0;
	
	int r_score = 0;
	for (int i = 0; i < look_size; i++) {
		if (l_labels.at<int>(i, 0))
			clust_one += 1;
		else
			clust_zero += 1;
		
		r_score += r_labels.at<int>(i, 0);
	}
	
	int best_label = -1;
	if (clust_one > clust_zero)
		best_label = 1;
	else
		best_label = 0;
	
	int best_r = 0;
	if (r_score > 2)
		best_r = 1;

	vector<Point2i> show_vecs;
	show_vecs.push_back(Point2i((int) l_centers.at<float>(best_label, 0), (int) l_centers.at<float>(best_label, 1)));
	show_vecs.push_back(Point2i((int) r_centers.at<float>(best_r, 0), (int) r_centers.at<float>(best_r, 1)));
	
	pt_shown.push_back(show_vecs);
}



void eyes_data::set_iris_size()
{

	
	int begin_search = 5;
	int init_l_size = (int) cur_eyes[0].cols / 6;
	int init_r_size = (int) cur_eyes[1].cols / 6;
	compute_gradients();
	
	create_phase_vectors();
	
	gx_list(0.5);
	

	vector<Mat> left, right;
	vector<double> iris_l, iris_r;
	iris_mats.push_back(left);
	iris_mats.push_back(right);
	
	double right_max = 0;
	double left_max = 0;
	double temp;
	int left_iris = 0;
	int right_iris = 0;
	int best_left = -1;
	int best_right = -1;
	
	for (int i = -begin_search; i <= 10; i++) {
		temp = create_iris_scores(0, init_l_size + i);
		iris_l.push_back(temp);
		if (temp > left_max) {
			left_max = temp;
			left_iris = i + init_l_size;
			best_left = i + begin_search;
		}
		
		temp = create_iris_scores(1, init_r_size + i);
		iris_r.push_back(temp);
		if (temp > right_max) {
			right_max = temp;
			right_iris = init_r_size + i;
			best_right = i + begin_search;
		}
		
		cout << "Done with Iris size: " << i << endl;
	}
	
	iris_max_score.push_back(iris_l);
	iris_max_score.push_back(iris_r);
	
	iris_size.push_back((double) left_iris);
	iris_size.push_back((double) right_iris);
	
	cur_scores.push_back(iris_mats[0][best_left]);
	cur_scores.push_back(iris_mats[1][best_right]);
}

void eyes_data::set_gx_threshold()
{
	//contrast_threshold.push_back(0.5 * l_max_gx);
	//contrast_threshold.push_back(0.5 * r_max_gx);
	
	double max = 0;
	int p_r;
	Point2i p_loc;
	
	for (int eye = 0; eye < cur_gx.size(); eye++) {
		double cur_max = 0;
		imshow("GX", reference_gx[eye]);
		waitKey(0);
		Mat test_gx = abs(reference_gx[eye]);
		imshow("abs gx", reference_gx[eye]);
		waitKey(0);
		p_r = iris_size[eye] + 5;
		p_loc = pupils[eye];
		for (int i = 0; i < test_gx.rows; i++) {
			for (int j = 0; j < test_gx.cols; j++) {
				if (i > p_loc.y - p_r && i < p_loc.y + p_r)
					continue;
				
				if (j > p_loc.x - p_r && j < p_loc.x + p_r)
					continue;
				
				
				if (test_gx.at<double>(i, j) > cur_max) {
					cout << test_gx.at<double>(i, j) << endl;
					cur_max = test_gx.at<double>(i, j);
				}
			
			}
		}
		
		cout << "the old gx: " << l_max_gx << " new: " << cur_max << endl;
	}
}

void eyes_data::all_scores_highest()
{
	for (int i = 0; i < cur_scores.size(); i++) {
		double max = 0;
		Point2i max_index(-1, -1);
		for (int y = 0; y < cur_scores[i].rows; y++) {
			for (int x = 0; x < cur_scores[i].cols; x++) {
				if (cur_scores[i].at<double>(y, x) > max) {
					max = cur_scores[i].at<double>(y, x);
					max_index.x = x;
					max_index.y = y;
				}
			}
		}
		
		pupils.push_back(max_index);
		abs_pupils.push_back(max_index);
		loc_scores.push_back(max);
	}
}


Mat eyes_data::create_eye_quilt(int eye_num)
{	
	int off_y = cur_eyes[eye_num].rows;
	int off_x = cur_eyes[eye_num].cols;
	
	Mat quilt = Mat::zeros(off_y * 2, off_x * 2, CV_8UC1);
	

	Mat char_score = norm_image_char(cur_scores[eye_num]);


	Mat char_gx = norm_image_char (cur_gx[eye_num]);

	Mat char_local = norm_image_char(cur_local_scores[eye_num]);
	

	/*imshow("cur eye", cur_eyes[eye_num]);
	waitKey(0);
	
	
	imshow("char gx", char_gx);
	waitKey(0);
	imshow("char score", char_score);
	waitKey(0);
	*/
	for (int i = 0; i < off_y; i++) {
		for (int j = 0; j < off_x; j++) {
			quilt.at<char>(i, j) = cur_eyes[eye_num].at<char>(i, j);
			quilt.at<char>(i, j + off_x) = char_gx.at<char>(i, j);
			quilt.at<char>(i + off_y, j) = char_score.at<char>(i, j);
			quilt.at<char>(i + off_y, j + off_x) = char_local.at<char>(i, j);
		}
	}
	
	cvtColor(quilt, quilt, CV_GRAY2BGR );
	
/*
	Point2i index = pupils[eye_num];
	Point2i abs_index = abs_pupils[eye_num];
	
	index.x += eye_index[eye_num].x;
	index.y += eye_index[eye_num].y;
	
	abs_index.x += eye_index[eye_num].x;
	abs_index.y += eye_index[eye_num].y;
	
	for (int lr = 0; lr < 2; lr++) {
		for (int ud = 0; ud < 2; ud++) {
			index.x += lr * off_x;
			index.y += ud * off_y;
			abs_index.x += lr * off_x;
			abs_index.y += ud * off_y;
			circle(cur_frame, index, 5, Scalar(0, 255, 0), -1, 8, 0);
			circle(cur_frame, abs_index, 5, Scalar(0, 0, 255), -1, 8, 0);
		}
	}*/
	

	return quilt;
}

class compare_local {
	public:
		bool operator()(gx_pt& a, gx_pt& b)
		{
			if (a.mag < b.mag)
				return true;
			else
				return false;
		}
};


/*
 * I use class gx_pt to hold the values in the priority queue
 * It is an implementation of gradient ascent
 */
 /*
void eyes_data::local_search(int prev_x, int prev_y, vector<gx_pt> &pt_list, Mat eye, int num_limit)
{
	priority_queue<gx_pt, vector<gx_pt>, compare_local> pq;
	
	
	double local_high = 0;//proposed_loc_score(prev_y, prev_x, pt_list);;
	Point2i local_index;
	pq.push(gx_pt(prev_x, prev_y, Point2d(0 , 0), local_high));
	
	Mat checked = Mat::zeros(eye.size(), CV_8UC1);
	
	checked.at<char>(prev_y, prev_x) = 255;
	
	int shift_x = consec_blinks + 1;
	int shift_y = consec_blinks + 1;
	
	Mat double_checked = Mat::zeros(eye.size(), CV_64FC1);
	for (int i = 0; i < num_limit; i++) {

		gx_pt cur_pt(pq.top().x, pq.top().y, Point2d(0, 0), pq.top().mag);
		pq.pop();
		
		if (cur_pt.mag > local_high) {
			cout << "Updating from: " << local_index << " to: ";
			local_high = cur_pt.mag;
			local_index.x = cur_pt.x;
			local_index.y = cur_pt.y;
			cout << local_index << endl;
		}
		
		for (int add_x = -1; add_x < 2; add_x++) {
			for (int add_y = -1; add_y < 2; add_y++) {
				
				
				if (checked.at<char>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)))
					continue;
				
				if (cur_pt.y + (add_y * shift_y) < 0 || cur_pt.x + (add_x * shift_x) < 0)
					continue;

				if (cur_pt.y + (add_y * shift_y) >= checked.rows || cur_pt.x + (add_x * shift_x) >= checked.cols)
					continue;

				checked.at<char>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)) = 255;
				double score = proposed_loc_score(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x), pt_list);
				double_checked.at<double>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)) = score;
				pq.push(gx_pt(cur_pt.x + (add_x * shift_x), cur_pt.y + (add_y * shift_y), Point2d(0 , 0), score));
			}
		}

	}
	
	cur_local_scores.push_back(double_checked);
	pupils.push_back(local_index);
	loc_scores.push_back(local_high);
}
*/
void eyes_data::local_search(int prev_x, int prev_y, vector<gx_pt> &pt_list, Mat eye, int num_limit, double iris_s)
{
	priority_queue<gx_pt, vector<gx_pt>, compare_local> pq;
	
	
	double local_high = 0;//proposed_loc_score(prev_y, prev_x, pt_list);;
	Point2i local_index;
	
	double initial_span;
	if (consec_blinks >= 4)
		initial_span = 32;
	else
		initial_span = 16;
		
	pq.push(gx_pt(prev_x, prev_y, Point2d(initial_span, 0), local_high));
	
	
	Mat checked = Mat::zeros(eye.size(), CV_8UC1);
	
	checked.at<char>(prev_y, prev_x) = 255;
	
	int shift_x = consec_blinks + 1;
	int shift_y = consec_blinks + 1;
	
	Mat double_checked = Mat::zeros(eye.size(), CV_64FC1);
	for (int i = 0; i < num_limit; i++) {

		gx_pt cur_pt(pq.top().x, pq.top().y, pq.top().phase, pq.top().mag);
		pq.pop();
		
		if (cur_pt.mag > local_high) {
			cout << "Updating from: " << local_index << " to: ";
			local_high = cur_pt.mag;
			local_index.x = cur_pt.x;
			local_index.y = cur_pt.y;
			cout << local_index << endl;
		}
		
		shift_x = (int) cur_pt.phase.x;
		shift_y = (int) cur_pt.phase.x;
		
		if (shift_x > 1) {
			double score = proposed_loc_score(cur_pt.y, cur_pt.x, pt_list);
			pq.push(gx_pt(cur_pt.x, cur_pt.y, Point2d((double) shift_x / 2 , 0), score));
		}
		
		for (int add_x = -1; add_x < 2; add_x++) {
			for (int add_y = -1; add_y < 2; add_y++) {
				
				
				if (checked.at<char>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)))
					continue;
				
				if (cur_pt.y + (add_y * shift_y) < 0 || cur_pt.x + (add_x * shift_x) < 0)
					continue;

				if (cur_pt.y + (add_y * shift_y) >= checked.rows || cur_pt.x + (add_x * shift_x) >= checked.cols)
					continue;

				checked.at<char>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)) = 255;
				double score = proposed_loc_score(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x), pt_list, iris_s);
				double_checked.at<double>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)) = 1;//score;
				if (shift_x > 1)
					pq.push(gx_pt(cur_pt.x + (add_x * shift_x), cur_pt.y + (add_y * shift_y), Point2d((double) shift_x / 2 , 0), score));
				else
					pq.push(gx_pt(cur_pt.x + (add_x * shift_x), cur_pt.y + (add_y * shift_y), Point2d(1 , 0), score));
			}
		}

	}
	
	cur_local_scores.push_back(double_checked);
	pupils.push_back(local_index);
	loc_scores.push_back(local_high);
}

void eyes_data::local_search_ref(int prev_x, int prev_y, vector<gx_pt> &pt_list, Mat eye, int num_limit, double iris_s, int l_r)
{
	priority_queue<gx_pt, vector<gx_pt>, compare_local> pq;
	
	
	double local_high = 0;//proposed_loc_score(prev_y, prev_x, pt_list);;
	Point2i local_index;
	
	double initial_span;
	if (consec_blinks >= 4)
		initial_span = 32;
	else
		initial_span = 16;
		
	pq.push(gx_pt(prev_x, prev_y, Point2d(initial_span, 0), local_high));
	
	Mat checked = Mat::zeros(eye.size(), CV_8UC1);
	
	checked.at<char>(prev_y, prev_x) = 255;
	Mat double_checked = Mat::zeros(eye.size(), CV_64FC1);
	
	if (consec_blinks) {
		Point2i ref = ref_pt[l_r];
		int span = 16;
		for (int x = -2; x < 3; x++) {
			for (int y = -1; y < 2; y++) {
				if (checked.at<char>(ref.y + (y * span), ref.x + (x * span)))
					continue;
				
				if (ref.y + (y * span) < 0 || ref.x + (x * span) < 0)
					continue;

				if (ref.y + (y * span) >= checked.rows || ref.x + (x * span) >= checked.cols)
					continue;
				
				checked.at<char>(ref.y + (y * span), ref.x + (x * span)) = 255;
				
				double score = proposed_loc_score(ref.y + (y * span), ref.x + (x * span), pt_list, iris_s);
				
				double_checked.at<double>(ref.y + (y * span), ref.x + (x * span)) = 1;
				
				pq.push(gx_pt(ref.x + (x * span), ref.y + (y * span), Point2d((double) span / 2, 0), score));
			}
		}
	}
	
	
	int shift_x = consec_blinks + 1;
	int shift_y = consec_blinks + 1;
	

	for (int i = 0; i < num_limit; i++) {

		gx_pt cur_pt(pq.top().x, pq.top().y, pq.top().phase, pq.top().mag);
		pq.pop();
		
		if (cur_pt.mag > local_high) {
			cout << "Updating from: " << local_index << " to: ";
			local_high = cur_pt.mag;
			local_index.x = cur_pt.x;
			local_index.y = cur_pt.y;
			cout << local_index << endl;
		}
		
		shift_x = (int) cur_pt.phase.x;
		shift_y = (int) cur_pt.phase.x;
		
		if (shift_x > 1) {
			double score = proposed_loc_score(cur_pt.y, cur_pt.x, pt_list);
			pq.push(gx_pt(cur_pt.x, cur_pt.y, Point2d((double) shift_x / 2 , 0), score));
		}
		
		for (int add_x = -1; add_x < 2; add_x++) {
			for (int add_y = -1; add_y < 2; add_y++) {
				
				
				if (checked.at<char>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)))
					continue;
				
				if (cur_pt.y + (add_y * shift_y) < 0 || cur_pt.x + (add_x * shift_x) < 0)
					continue;

				if (cur_pt.y + (add_y * shift_y) >= checked.rows || cur_pt.x + (add_x * shift_x) >= checked.cols)
					continue;

				checked.at<char>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)) = 255;
				double score = proposed_loc_score(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x), pt_list, iris_s);
				double_checked.at<double>(cur_pt.y + (add_y * shift_y), cur_pt.x + (add_x * shift_x)) = 1;//score;
				if (shift_x > 1)
					pq.push(gx_pt(cur_pt.x + (add_x * shift_x), cur_pt.y + (add_y * shift_y), Point2d((double) shift_x / 2 , 0), score));
				else
					pq.push(gx_pt(cur_pt.x + (add_x * shift_x), cur_pt.y + (add_y * shift_y), Point2d(1 , 0), score));
			}
		}

	}
	
	cur_local_scores.push_back(double_checked);
	pupils.push_back(local_index);
	loc_scores.push_back(local_high);
}

void eyes_data::draw_face()
{
	if (cur_l_gx > threshold_const * l_max_gx)
		rectangle (cur_frame, eye_rects[0], Scalar(255, 0, 0), 5);
	else
		rectangle (cur_frame, eye_rects[0], Scalar(0, 0, 255), 5);
	
	if (cur_r_gx > threshold_const * r_max_gx)
		rectangle (cur_frame, eye_rects[1], Scalar(255, 0, 0), 5);
	else	
		rectangle (cur_frame, eye_rects[1], Scalar(0, 0, 255), 5);
		
	if (cur_l_gx < threshold_const * l_max_gx && cur_r_gx < threshold_const * r_max_gx)
		return;

	for (int i = 0; i < cur_eyes.size(); i++) {
		Point2i index;
		if (pt_shown.size() == 0)
			index = pupils[i];
		else
			index = pt_shown.back()[i];
		
		index.x += eye_index[i].x;
		index.y += eye_index[i].y;
		
		circle(cur_frame, index, 5, Scalar(0, 255, 0), -1, 8, 0);
	}
}


void eyes_data::create_eye_vid(string eye_vid_name, vector<vector<Mat> > &video_frames)
{
	
	cout << "IN creater eye" << endl;
	cout << eye_vid_name + "_left.avi" << endl;
		
	VideoWriter left_vid(eye_vid_name + "_left.avi", CV_FOURCC('D', 'I', 'V', 'X'), 30, video_frames[0][0].size(), false);
	VideoWriter right_vid(eye_vid_name + "_right.avi",  CV_FOURCC('D', 'I', 'V', 'X'), 30, video_frames[0][1].size(), false);
	
	if (!left_vid.isOpened())
		cout << "WHAT THE FUCKKK" << endl;
	Size left_size = video_frames[0][0].size();
	Size right_size = video_frames[0][1].size();
	
	cout << left_size << endl;
	cout << right_size << endl;
	cout << video_frames.size() << endl;
	cout << video_frames[0].size() << endl;
	for (int i = 0; i < video_frames.size(); i++) {
		if (left_size.width == video_frames[i][0].cols && left_size.height == video_frames[i][0].rows)
			left_vid << video_frames[i][0];
		else
			cout << "Wrong left: " << video_frames[i][0].size() << endl;
			
		if (right_size.width == video_frames[i][1].cols && right_size.height == video_frames[i][1].rows)
			right_vid << video_frames[i][1];
		else
			cout << "Wrong right: " << video_frames[i][1].size() << endl;
	}
}

vector<Point2i> pupil_locations(vector<Mat> g_xs, vector<Mat> g_ys, vector<Mat> g_mags, vector<Mat> eyes, eyes_data &data)
{
	vector<Point2i> pupil_locs;
	
	vector<Mat> score_data;
	for (int eye = 0; eye < g_xs.size(); eye++) {
		
		vector<vector<Point2d> > phase = phase_vectors(g_xs[eye], g_ys[eye], g_mags[eye]);
		
		//Mat norm_gx = norm_image(g_xs[eye]);
		Mat test_gx = abs_threshold_mag(g_xs[eye], 0.5);
		
	//	imshow("One eye", test_gx);
		
	//	waitKey(0);
		//Mat scores = pupil_scores(phase, eyes[eye], g_xs[eye]);
		
		Mat test_scores = test_pupil_scores(phase, eyes[eye], test_gx, 35);
		
		Mat scores = test_scores;
		
		double max = 0;
		double test_max = 0;
		Point2i index, test_index;
		for (int i = 0; i < scores.rows; i++) {
			for (int j = 0; j < scores.cols; j++) {

				if (test_scores.at<double>(i, j) > test_max) {
					test_index.x = j;
					test_index.y = i;
					test_max = test_scores.at<double>(i, j);
				}
			}
		}
		
		pupil_locs.push_back(test_index);
		
		//score_data.push_back(test_scores);
		
	
		
		//Mat norm_test_scores = norm_image(test_scores);
		
		Mat norm_test_scores = norm_image_char(test_scores);
		
		//imshow("score", norm_test_scores);
		//waitKey(0);
		
		score_data.push_back(norm_test_scores);
		
		continue;
		
		cout << "Test max: " << test_max << endl;
		
		imshow("test norm", norm_test_scores);
		
		waitKey(0);
		
		Mat norm_mag = norm_image(g_mags[eye]);
		
		imshow("Mag", norm_mag);
		
		waitKey(0);
		
		Mat norm_scores = norm_image(scores);
		
		imshow("Norm score", norm_scores);
		
		waitKey(0);
		
		
		pupil_locs.push_back(test_index);
	}
	
	

	data.pupil_loc_scores.push_back(score_data);
	
	return pupil_locs;
}

gx_pt::gx_pt (int pos_x, int pos_y, Point2d p, double gx_mag)
{
	x = pos_x;
	y = pos_y;
	phase = p;
	mag = gx_mag;
}

void eyes_data::create_phase_vectors ()
{
	cur_left_phase = phase_vectors(cur_gx[0], cur_gy[0], cur_mag[0]);
	cur_right_phase = phase_vectors(cur_gx[1], cur_gy[1], cur_mag[1]);
}

void eyes_data::create_filtered_gx(double thresh)
{
	vector<Mat> filtered_gx;
	for (int eye = 0; eye < cur_gx.size(); eye++) {
		Mat test_gx = abs(cur_gx[eye]);
		test_gx = threshold_mag(test_gx, thresh);
		
		filtered_gx.push_back(test_gx);
	}
	
	cur_gx = filtered_gx;
	gx.push_back(filtered_gx);
}

void eyes_data::gx_list (double threshold = 0.5)
{
	double max = 0;
	
	for (int eye = 0; eye < cur_gx.size(); eye++) {
		Mat test_gx = abs(cur_gx[eye]);
		if (reference_gx.size() < 2)
			reference_gx.push_back(cur_gx[eye]);
			
		max = 0;
		test_gx = norm_image_with_max(test_gx, max);
		if (eye == 0) {
			if (l_max_gx)
				cur_l_gx = max;
			else {
				l_max_gx = max;
				cur_l_gx = max;
			}
		} else {
			if (r_max_gx)
				cur_r_gx = max;
			else {
				cur_r_gx = max;
				r_max_gx = max;
			}
		}
		
		for (int i = 0; i < test_gx.rows; i++) {
			for (int j = 0; j < test_gx.cols; j++) {
				if (test_gx.at<double>(i, j) >= threshold) {
					if (eye == 0)
						left_pts.push_back(gx_pt(j, i, cur_left_phase[i][j], cur_mag[0].at<double>(i, j)));
					else
						right_pts.push_back(gx_pt(j, i, cur_right_phase[i][j], cur_mag[1].at<double>(i, j)));
					
					
				} else {
					test_gx.at<double>(i, j) = 0;
				}
			}
		}
		
		cur_gx[eye] = test_gx;
		/*
		if (eye) {
			imshow("Filtered", test_gx);
			waitKey(0);
		}*/
	}
}

void eyes_data::gx_list_gradient_theshold ()
{
	for (int eye = 0; eye < cur_gx.size(); eye++) {
		Mat test_gx = abs(cur_gx[eye]);
		double cur_max = 0;
		
		double thresh;
		if (eye)
			thresh = r_max_gx * 0.5;
		else
			thresh = l_max_gx * 0.5;
		
		for (int i = 0; i < test_gx.rows; i++) {
			for (int j = 0; j < test_gx.cols; j++) {
				if (test_gx.at<double>(i, j) > cur_max)
					cur_max = test_gx.at<double>(i, j);

				if (test_gx.at<double>(i, j) >= thresh) {
					if (eye == 0)
						left_pts.push_back(gx_pt(j, i, cur_left_phase[i][j], cur_mag[0].at<double>(i, j)));
					else
						right_pts.push_back(gx_pt(j, i, cur_right_phase[i][j], cur_mag[1].at<double>(i, j)));
					
					
				} else {
					test_gx.at<double>(i, j) = 0;
				}
			}
		}
		
		if (eye)
			cur_r_gx = cur_max;
		else
			cur_l_gx = cur_max;
		
		cur_gx[eye] = test_gx;
		
	}
}


void eyes_data::compute_gradients ()
{
	for (int i = 0; i < cur_eyes.size(); i++) {
		Mat e = cur_eyes[i];
		
		Mat temp_gy = compute_y_gradient(e);
		cur_gy.push_back(temp_gy);

		Mat temp_gx = compute_x_gradient(e);
		
		cur_gx.push_back(temp_gx);

		Mat g_mag = compute_g_mag(temp_gx, temp_gy);
		cur_mag.push_back(g_mag);
	}
}

void eyes_data::create_distance_mat()
{

	int y_size = cur_eyes[0].rows > cur_eyes[1].rows ? cur_eyes[0].rows : cur_eyes[1].rows;
	int x_size = cur_eyes[0].cols > cur_eyes[1].cols ? cur_eyes[0].cols : cur_eyes[1].cols;
	
	x_dis = Mat::zeros(y_size, x_size, CV_64FC1);
	y_dis = Mat::zeros(y_size, x_size, CV_64FC1);
	total_dis = Mat::zeros(y_size, x_size, CV_64FC1);
	
	for (int y_pos = 0; y_pos < y_size; y_pos++) {
		for (int x_pos = 0; x_pos < x_size; x_pos++) {
			if (y_pos == 0 && x_pos == 0)
				continue;

			double x_mag = (double) x_pos;
			double y_mag = (double) y_pos;
			
			double mag = sqrt((x_pos * x_pos) + (y_pos * y_pos));
			
			total_dis.at<double>(y_pos, x_pos) = mag;
			x_dis.at<double>(y_pos, x_pos) = x_mag / mag;
			y_dis.at<double>(y_pos, x_pos) = y_mag / mag;
		}
	}
}

double eyes_data::proposed_loc_score(int p_y, int p_x, vector<gx_pt> &pts, double iris_s)
{
	double score = 0;
	//cout << pts.size() << endl;
	for (int cur_pt = 0; cur_pt < pts.size(); cur_pt++) {
		if (cur_pt >= pts.size()) {
			cout << "CUR PT: " << endl;
			abort();
		}
		
		if (p_y == pts[cur_pt].y && p_x == pts[cur_pt].x)
			continue;
		
		int abs_x_dis = abs(pts[cur_pt].x - p_x);
		int abs_y_dis = abs(pts[cur_pt].y - p_y);
		
		Point2d dis;
		
		if (abs_y_dis >= x_dis.rows || abs_y_dis >= y_dis.rows) { 
			cout << x_dis.size() << endl;
			cout << y_dis.size() << endl;
			cout << abs_y_dis << endl;
			cout << p_y << " " << p_x << endl;
			cout << "ABS Y DIS" << endl;
			abort();
		}
		
		if ( abs_y_dis >= total_dis.rows) {
			cout << "ABS Y TOTAL" << endl;
			abort();
		}
		
		if (abs_x_dis >= x_dis.cols || abs_x_dis >= y_dis.cols || abs_x_dis >= total_dis.cols) {
			cout << "ABS X DIS" << endl;
			abort();
		}
		
		dis.x = pts[cur_pt].x < p_x ? -x_dis.at<double>(abs_y_dis, abs_x_dis) : x_dis.at<double>(abs_y_dis, abs_x_dis);
		dis.y = pts[cur_pt].y < p_y ? -y_dis.at<double>(abs_y_dis, abs_x_dis) : y_dis.at<double>(abs_y_dis, abs_x_dis);
		
		score += dot(dis, pts[cur_pt].phase) * gauss_set_dis(total_dis.at<double>(abs_y_dis, abs_x_dis), iris_s);// * pts[cur_pt].mag;
	}
	
	return score;
}

double print_time_passed(int64 start, int64 end)
{
	double freq = getTickFrequency();
	double total_ticks = (double) end - start;
	
	cout << "Time passed: " << total_ticks / freq << endl;
	
}

double eyes_data::create_iris_scores(int eye, int iris_size)
{
	Mat score = Mat::zeros(cur_eyes[eye].size(), CV_64FC1);
	double highest_score = 0;
	double temp;
	for (int i = 0; i < score.rows; i++) {
		
		for (int j = 0; j < score.cols; j++) {

			if (eye == 0)
				temp = proposed_loc_score(i, j, left_pts, (double) iris_size);
			else
				temp = proposed_loc_score(i, j, right_pts, (double) iris_size);

			score.at<double>(i, j) = temp;
			if (temp > highest_score)
				highest_score = temp;
		}
	}
	
	iris_mats[eye].push_back(score);
	
	return highest_score;
}

void eyes_data::create_pupil_scores_all()
{

	for (int eye = 0; eye < 2; eye++) {
		Mat score = Mat::zeros(cur_eyes[eye].size(), CV_64FC1);
		for (int i = 0; i < score.rows; i++) {
			

			for (int j = 0; j < score.cols; j++) {

				if (eye == 0)
					score.at<double>(i, j) = proposed_loc_score(i, j, left_pts);
				else
					score.at<double>(i, j) = proposed_loc_score(i, j, right_pts);
				

				
			}
		}/*
		if (eye == 1) {
			Mat temp = norm_image_char(score);
			imshow("Right", temp);
			waitKey(0);
			Mat temp_f = norm_image_char(cur_gx[1]);
			imshow("Right Filt", temp_f);
			waitKey(0);
			
		}*/
		cur_scores.push_back(score);
	}
	
}

void eyes_data::wrapper() {
	
	compute_gradients();
	
	create_phase_vectors();
	
	gx_list(0.5);
	
	create_pupil_scores_all();
	
	all_scores_highest();
}

void eyes_data::local_wrapper() {
	
	compute_gradients();
	
	create_phase_vectors();
	
	gx_list(0.5);
	
	if (l_max_gx * threshold_const < cur_l_gx || r_max_gx * threshold_const < cur_r_gx) {
		consec_blinks = 0;
		//local_search(prev_pupils[0].x, prev_pupils[0].y, left_pts, cur_eyes[0], 100, iris_size[0]);
		//local_search(prev_pupils[1].x, prev_pupils[1].y, right_pts, cur_eyes[1], 100, iris_size[1]);
		local_search_ref(prev_pupils[0].x, prev_pupils[0].y, left_pts, cur_eyes[0], 100, iris_size[0], 0);
		local_search_ref(prev_pupils[1].x, prev_pupils[1].y, right_pts, cur_eyes[1], 100, iris_size[1], 1);
	} else {
		pupils.push_back(prev_pupils[0]);
		pupils.push_back(prev_pupils[1]);
		consec_blinks++;
	}
}


void eyes_data::local_wrapper_ref_thresh() {
	
	compute_gradients();
	
	create_phase_vectors();
	
	gx_list_gradient_theshold();
	
	
	if (l_max_gx * threshold_const < cur_l_gx || r_max_gx * threshold_const < cur_r_gx) {
		if (consec_blinks)
			consec_blinks -= 1;

		local_search(prev_pupils[0].x, prev_pupils[0].y, left_pts, cur_eyes[0], 100, iris_size[0]);
		local_search(prev_pupils[1].x, prev_pupils[1].y, right_pts, cur_eyes[1], 100, iris_size[1]);
	
	} else {
		pupils.push_back(prev_pupils[0]);
		pupils.push_back(prev_pupils[1]);
		consec_blinks++;
	}
	
}

void eyes_data::search_features() {
	
	compute_gradients();
	
	create_phase_vectors();
	
	gx_list(0.5);
}

void eyes_data::search_features_ref_thresh() {
	
	compute_gradients();
	
	create_phase_vectors();
	
	gx_list_gradient_theshold();
}

void eyes_data::compute_both_searches ()
{	
	if (l_max_gx * threshold_const < cur_l_gx || r_max_gx * threshold_const < cur_r_gx) {
		
		if (largest_blinks < consec_blinks) {
			largest_blinks = consec_blinks;
		}
		
		local_search(prev_pupils[0].x, prev_pupils[0].y, left_pts, cur_eyes[0], 100);
		local_search(prev_pupils[1].x, prev_pupils[1].y, right_pts, cur_eyes[1], 100);
		consec_blinks = 0;
	} else {
		pupils.push_back(prev_pupils[0]);
		pupils.push_back(prev_pupils[1]);
		Mat double_checked = Mat::zeros(cur_eyes[0].size(), CV_64FC1);
		double_checked.at<double>(prev_pupils[0].x, prev_pupils[0].y) = 1;
		cur_local_scores.push_back(double_checked);
		Mat right_checked =  Mat::zeros(cur_eyes[1].size(), CV_64FC1);
		right_checked.at<double>(prev_pupils[1].x, prev_pupils[1].y) = 1;
		cur_local_scores.push_back(right_checked);
		consec_blinks += 1;
	}
	
	
	
	create_pupil_scores_all();
	
	all_scores_highest();
	
	
}

vector<Point2i> eyes_data::pupil_locations(vector<Mat> g_xs, vector<Mat> g_ys, vector<Mat> g_mags, vector<Mat> eyes)
{
	vector<Point2i> pupil_locs;
	
	vector<Mat> score_data;
	vector<Mat> filtered_gx;
	
	for (int eye = 0; eye < g_xs.size(); eye++) {
		
		vector<vector<Point2d> > phase = phase_vectors(g_xs[eye], g_ys[eye], g_mags[eye]);
		
		//Mat norm_gx = norm_image(g_xs[eye]);
		Mat test_gx = abs_threshold_mag(g_xs[eye], 0.5);
		
		
	//	imshow("One eye", test_gx);
		
	//	waitKey(0);
		//Mat scores = pupil_scores(phase, eyes[eye], g_xs[eye]);
		
		Mat test_scores = test_pupil_scores(phase, eyes[eye], test_gx, 35);
		Mat norm_test_scores = norm_image_char(test_scores);
		if (eye == -1) {
			test_gx = norm_image_char(test_gx);
			imwrite("./videos/input_gx.jpg", test_gx);
			imwrite("./videos/gauss_heat_map.jpg", norm_test_scores);

		}
		Mat scores = test_scores;
		
		double max = 0;
		double test_max = 0;
		Point2i index, test_index;
		for (int i = 0; i < scores.rows; i++) {
			for (int j = 0; j < scores.cols; j++) {
				
				if (test_scores.at<double>(i, j) > test_max) {
					test_index.x = j;
					test_index.y = i;
					test_max = test_scores.at<double>(i, j);
				}
			}
		}
		
		pupil_locs.push_back(test_index);
		
		//score_data.push_back(test_scores);
		
	
		
		//Mat norm_test_scores = norm_image(test_scores);	
		//imshow("score", norm_test_scores);
		//waitKey(0);
		
		score_data.push_back(norm_test_scores);
	
	}
	
	return pupil_locs;
}

int alt_main (int argc, char *argv[])
{
	vector<Mat> eyes;
	vector<Rect> faces;
	vector<vector<Rect> > eye_rects;
	vector<Point2i> eye_index;
	
	if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };
	if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };
	
	string image_name = argv[1];
	string file_path = argv[2];
	
	cout << image_name << endl << file_path << endl;
	
	Mat face_frame = imread(image_name, CV_LOAD_IMAGE_COLOR);
	Mat orig_face = imread(image_name, CV_LOAD_IMAGE_COLOR);
	
	eyes = detectAndDisplay(face_frame, "sultan", eye_index, faces, eye_rects);
	
	Point2i im_size((int) face_frame.cols, (int) face_frame.rows);
	face_eye_detection_filtering(eye_index, eyes, faces, im_size, eye_rects);
	
	draw_face(orig_face, faces, eye_rects[0]);
	
	imwrite(file_path + "face_detection.jpg", orig_face);
	
	left_right_eyes(eyes, eye_index, eye_rects);
	
	for (int i = 0; i < eyes.size(); i++) {
		string direc;
		if (i == 0)
			direc = "left";
		else
			direc = "right";
			
		Mat e;
		
		e = eyes[i];
		imwrite(file_path + "eye_" + direc + ".jpg", e);
		Mat gy = compute_y_gradient(e);
		
		Mat norm_gy = norm_image(gy);
		imwrite(file_path + "ygrad_" + direc + ".jpg", norm_gy);
		Mat gx = compute_x_gradient(e);
		Mat norm_gx = norm_image(gx);
		imwrite(file_path + "xgrad_" + direc + ".jpg", norm_gx);
		Mat g_mag = compute_g_mag(gx, gy);
		Mat norm_gmag = norm_image(g_mag);
		imwrite(file_path + "mag_" + direc + ".jpg", norm_gmag);
		
		Mat g_phase = compute_g_phase(gx, gy);
		Mat norm_gphase = norm_image(g_phase);
		imwrite(file_path + "phase_" + direc + ".jpg", norm_gphase);
	}
}


void left_right_eyes(vector<Mat> &eyes, vector<Point2i> &eye_locs, vector<vector<Rect> > eye_rects)
{
	if (eye_locs[0].x < eye_locs[1].x)
		return;
	
	Mat temp_mat = eyes[0];
	Point2i temp_pt = eye_locs[0];
	Rect temp_rect = eye_rects[0][0];
	
	eyes[0] = eyes[1];
	eye_locs[0] = eye_locs[1];
	eye_rects[0][0] = eye_rects[0][1];
	
	eyes[1] = temp_mat;
	eye_locs[1] = temp_pt;
	eye_rects[0][1] = temp_rect;
}

void draw_face(Mat &frame, vector<Rect> faces, vector<Rect> eyes)
{
	int i = 0;
	Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
	ellipse( frame, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
	
	for( size_t j = 0; j < eyes.size(); j++ )
	{
		Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
		int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
		circle( frame, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
	}
}

int find_best_face(vector<Point2i> eyes, vector<vector<int> > &eye_indices, vector<Rect> faces, Point2i im_size, vector<vector<Rect> > &eye_rects)
{
	vector<int> pos;
	for (int i = 0; i < eye_indices.size(); i++) {
		if (eye_indices[i].size() >= 2)
			pos.push_back(i);
	}
	
	if (pos.size() > 1) {
		int most_center;
		double mag_to_cent = (double) (im_size.x + im_size.y);
		
		Point2d center((double)im_size.x / 2, (double) im_size.y / 2);
		for (int i = 0; i < pos.size(); i++) {
			Point2i f_c;
			f_c.x = faces[pos[i]].x + (0.5 * faces[pos[i]].width);
			f_c.y = faces[pos[i]].y + (0.5 * faces[pos[i]].height);
			
			Point2d dif;
			dif.x = (double) f_c.x - center.x;
			dif.y = (double) f_c.y - center.y;
			
			double mag = sqrt((dif.x * dif.x) + (dif.y + dif.y));
			
			if (mag < mag_to_cent) {
				most_center = i;
				mag_to_cent = mag;
			}
		}
		
		vector<int> new_pos;
		new_pos.push_back(pos[most_center]);
		pos = new_pos;
	} 
		
	//lease squares difference between horizontal line eventually maybe
	if (eye_indices[pos[0]].size() > 2) {
		while (eye_indices[pos[0]].size() > 2) {
			int highest  = 0;
			Point2i l(0, 0);
			for (int i = 0; i < eye_indices[pos[0]].size(); i++) {
				if (eyes[eye_indices[pos[0]][i]].y > l.y) {
					l = eyes[eye_indices[pos[0]][i]];
					highest = i;
				}
			}
			eye_indices[pos[0]].erase(eye_indices[pos[0]].begin() + highest);
			eye_rects[pos[0]].erase(eye_rects[pos[0]].begin() + highest);
		}
	}
	
	return pos[0];
}

void face_eye_detection_filtering(vector<Point2i> &eyes, vector<Mat> &eye_mat, vector<Rect> &faces, Point2i im_size, vector<vector<Rect> > &eye_rects)
{
	vector<int> eyes_in_face;
	vector<vector<int> > eye_indices;
	
	cout << "face eye detection" << endl;
	
	for (int i = 0; i < faces.size(); i++) {
		int eye_num = 0;
		vector<int> eye_index;
		for (int j = 0; j < eyes.size(); j++) {
			cout << eyes[j] << endl;
			if (faces[i].x < eyes[j].x && faces[i].y < eyes[j].y && (faces[i].x + faces[i].width) > eyes[i].x && (faces[i].y + faces[i].height) > eyes[i].y) {
				eye_num++;
				eye_index.push_back(j);
			}
		}
		cout << eye_index.size() << endl;
		eye_indices.push_back(eye_index);
		eyes_in_face.push_back(eye_num);
	}
	
	
	int best_face = find_best_face(eyes, eye_indices, faces, im_size, eye_rects);
	
	vector<Point2i> new_eyes;
	vector<Mat> new_eye_mat;
	vector<Rect> face;
	vector<vector<Rect> > new_e_rects; 
	vector<Rect> two_e;
	face.push_back(faces[best_face]);
	for (int i = 0; i < eye_indices[best_face].size(); i++) {
		new_eyes.push_back(eyes[eye_indices[best_face][i]]);
		new_eye_mat.push_back(eye_mat[eye_indices[best_face][i]]);
		two_e.push_back(eye_rects[best_face][i]);
	}
	
	new_e_rects.push_back(two_e);
	
	eyes = new_eyes;
	eye_mat = new_eye_mat;
	faces = face;
	eye_rects = new_e_rects;
}

Mat threshold_mag(Mat mag, double threshold)
{
	Mat norm = norm_image(mag);
	
	for (int i = 0; i < norm.rows; i++) {
		for (int j = 0; j < norm.cols; j++) {
			if (norm.at<double>(i, j) < threshold) {
				norm.at<double>(i, j) = 0;
			}
		}
	}
	
	return norm;
}

Mat abs_threshold_mag(Mat mag, double threshold)
{
	Mat abs_mag = abs(mag);
	
	return threshold_mag(abs_mag, threshold);
}

Mat compute_g_mag(Mat gx, Mat gy)
{
	Mat g_mag(gx.size(), CV_64FC1);
	
	for (int i = 0; i < gx.rows; i++) {
		for (int j = 0; j < gx.cols; j++) {
			double x_mag = gx.at<double>(i, j);
			double y_mag = gy.at<double>(i, j);
			
			g_mag.at<double>(i, j) = sqrt((x_mag * x_mag) + (y_mag * y_mag));
		}
	}
	
	return g_mag;
}

Mat compute_g_phase(Mat gx, Mat gy)
{
	Mat g_phase(gx.size(), CV_64FC1);
	
	for (int i = 0; i < gx.rows; i++) {
		for (int j = 0; j < gx.cols; j++) {
			double x_mag = gx.at<double>(i, j);
			double y_mag = gy.at<double>(i, j);
			
			g_phase.at<double>(i, j) = atan2(y_mag, x_mag);
		
		}
	}
	
	return g_phase;
}

vector<vector<Point2d> > phase_vectors(Mat gx, Mat gy, Mat g_mag)
{
	Mat g_phase(gx.size(), CV_64FC1);
	
	vector<vector<Point2d> > phase;
	for (int i = 0; i < gx.rows; i++) {
		vector<Point2d> row;
		for (int j = 0; j < gx.cols; j++) {
			Point2d pt;
			if (g_mag.at<double>(i, j) == 0) {
				pt.x = -1;
				pt.y = -1;
				row.push_back(pt);
				continue;
			}
			
			double x_mag = gx.at<double>(i, j);
			double y_mag = gy.at<double>(i, j);
			
			double mag = sqrt((x_mag * x_mag) + (y_mag * y_mag));
			
			
			pt.x = x_mag / mag;
			pt.y = y_mag / mag;
			row.push_back(pt);
		}
		
		phase.push_back(row);
	}
	
	return phase;
}

Point2d norm_mag(Point2d a)
{
	double mag = sqrt((a.x * a.x) + (a.y * a.y));
	
	Point2d b;
	b.x = a.x / mag;
	b.y = a.y / mag;
	
	return b;
}

void norm_phase_vector(vector<vector<Point2d> > &phase)
{
	for (int i = 0; i < phase.size(); i++) {
		for (int j = 0; j < phase[0].size(); j++) {
			phase[i][j] = norm_mag(phase[i][j]);
		}
	}
}

double squared_dot(Point2d a, Point2d b)
{
	double c = (a.x * b.x) + (a.y * b.y);
	c = c * c;
	return c;
}

void create_vector_distance(int y_size, int x_size, Mat &distance, Mat &x_vec, Mat &y_vec)
{
	distance = Mat::zeros(y_size, x_size, CV_64FC1);
	x_vec = Mat::zeros(y_size, x_size, CV_64FC1);
	y_vec = Mat::zeros(y_size, x_size, CV_64FC1);
	
	for (int y_pos = 0; y_pos < y_size; y_pos++) {
		for (int x_pos = 0; x_pos < x_size; x_pos++) {
			if (y_pos == 0 && x_pos == 0)
				continue;
			
			double x_mag = (double) x_pos;
			double y_mag = (double) y_pos;
			
			double mag = sqrt((x_pos * x_pos) + (y_pos * y_pos));
			
			distance.at<double>(y_pos, x_pos) =  mag;
			x_vec.at<double>(y_pos, x_pos) = x_mag / mag;
			y_vec.at<double>(y_pos, x_pos) = y_mag / mag;
		}
	}
}

double dot(Point2d a, Point2d b)
{
	return (a.x * b.x) + (a.y * b.y); 
}

Mat pupil_scores(vector<vector<Point2d> > phase, Mat image, Mat g_mag)
{
	Mat scores = Mat::zeros(phase.size(), phase[0].size(), CV_64FC1);

	Mat distance, x_vec, y_vec;
	create_vector_distance((int) phase.size(), (int) phase[0].size(), distance, x_vec, y_vec);
	
	for (int p_y = 0; p_y < phase.size(); p_y++) {
		cout << p_y << endl;
		for (int p_x = 0; p_x < phase[0].size(); p_x++) {
			double score = 0;
			
			double color_cost = 1 / (double(image.at<char>(p_y, p_x)) + 1);

			for (int l_y = 0; l_y < phase.size(); l_y++) {
				for (int l_x = 0; l_x < phase[0].size(); l_x++) {
					if (p_y == l_y && p_x == l_x)
						continue;
						
					
					if (phase[l_y][l_x].x == -1 && phase[l_y][l_x].y == -1)
						continue;
					
					int abs_x_dis = abs(l_x - p_x);
					int abs_y_dis = abs(l_y - p_y);
					
					Point2d dis;
					
					dis.x = l_x < p_x ? -x_vec.at<double>(abs_y_dis, abs_x_dis) : x_vec.at<double>(abs_y_dis, abs_x_dis);
					dis.y = l_y > p_y ? -y_vec.at<double>(abs_y_dis, abs_x_dis) : y_vec.at<double>(abs_y_dis, abs_x_dis);
					
					score += fabs(dot(dis, phase[l_y][l_x])) * color_cost;
				}
			}
			
			scores.at<double>(p_y, p_x) = score;
					
		}
	}

	return scores;
}

double gauss_set_dis(double dis, double offset)
{
	double x = dis - offset;
	x = x * x;
	
	return exp (-x/ 4.0);
}

double gauss_dis(double dis)
{
	double x = dis - 23;
	x = x * x;
	
	return exp (-x/ 4.0);
}
			
Mat test_pupil_scores(vector<vector<Point2d> > phase, Mat image, Mat g_mag, int scan_window)
{
	Mat scores = Mat::zeros(phase.size(), phase[0].size(), CV_64FC1);

	Mat distance, x_vec, y_vec;
	create_vector_distance((int) phase.size(), (int) phase[0].size(), distance, x_vec, y_vec);
	
	cout << "test pupil scores" << endl;
	for (int p_y = 0; p_y < phase.size(); p_y++) {

		for (int p_x = 0; p_x < phase[0].size(); p_x++) {
			

			double score = 0;
			
			double color_cost = 1 / (double(image.at<char>(p_y, p_x)) + 1);

			for (int l_y = 0; l_y < phase.size(); l_y++) {
				for (int l_x = 0; l_x < phase[0].size(); l_x++) {
					if (g_mag.at<double>(l_y, l_x) == 0)
						continue;
						
					/*if (p_y - scan_window >= l_y || p_y + scan_window <= l_y)
						continue;
					
					if (p_x - scan_window >= l_x || p_x + scan_window <= l_x)
						continue;
					
					if (phase[l_y][l_x].x == -1 && phase[l_y][l_x].y == -1)
						continue;
					*/	
					if (p_y == l_y && p_x == l_x)
						continue;	
					
					int abs_x_dis = abs(l_x - p_x);
					int abs_y_dis = abs(l_y - p_y);
					
					Point2d dis;
					
					dis.x = l_x < p_x ? -x_vec.at<double>(abs_y_dis, abs_x_dis) : x_vec.at<double>(abs_y_dis, abs_x_dis);
					dis.y = l_y < p_y ? -y_vec.at<double>(abs_y_dis, abs_x_dis) : y_vec.at<double>(abs_y_dis, abs_x_dis);
					
					//score += dot(dis, phase[l_y][l_x]) * color_cost * fabs(g_mag.at<double>(l_y, l_x)) ;
					//score += color_cost;
					//score += 1;
					//score += dot(dis, phase[l_y][l_x]) * fabs(g_mag.at<double>(l_y, l_x));// * gauss_dis(sqrt((abs_x_dis) * 2 + (abs_y_dis) * 2));
					score += dot(dis, phase[l_y][l_x]) * gauss_dis(sqrt((abs_x_dis * abs_x_dis) + (abs_y_dis * abs_y_dis))) * fabs(g_mag.at<double>(l_y, l_x));
				}
			}
			
		
			scores.at<double>(p_y, p_x) = score; // * color_cost;
					
		}
	}
	cout << "done with scores" << endl;
	return scores;
}
			

Mat smooth_image(Mat input, int ksize, double sigma)
{
	
	Mat output;
	
	Size s(ksize, ksize);
	
	GaussianBlur(input, output, s, sigma);
	
	return output;
}

Mat compute_y_gradient(Mat input)
{
	Mat x_grad = Mat::zeros(3, 3, CV_64FC1);
	
	x_grad.at<double>(0, 0) = -1;
	x_grad.at<double>(0, 2) = -1;
	x_grad.at<double>(0, 1) = -2;
	x_grad.at<double>(2, 1) = 2;
	x_grad.at<double>(2, 0) = 1;
	x_grad.at<double>(2, 2) = 1;
	
	Mat grad_im;
	
	filter2D(input, grad_im, CV_64F, x_grad);
	
	return grad_im;
}

Mat compute_x_gradient(Mat input)
{
	Mat x_grad = Mat::zeros(3, 3, CV_64FC1);
	
	x_grad.at<double>(0, 0) = -1;
	x_grad.at<double>(0, 2) = 1;
	x_grad.at<double>(1, 0) = -2;
	x_grad.at<double>(1, 2) = 2;
	x_grad.at<double>(2, 0) = -1;
	x_grad.at<double>(2, 2) = 1;
	
	Mat grad_im;
	
	filter2D(input, grad_im, CV_64F, x_grad);
	
	return grad_im;
}

Mat norm_image(Mat input)
{
	double max = 0;
	
	double min = 1000000;
	
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			if (input.at<double>(i, j) > max)
				max = input.at<double>(i, j);
			if (input.at<double>(i, j) < min)
				min = input.at<double>(i, j);
		}
	}
	
	Mat normalized(input.size(), CV_64FC1);
	cout << "Norm image" << endl;
	
	cout << "Max gx: " << max << endl;
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			normalized.at<double>(i, j) = ((input.at<double>(i, j) - min) / (max - min));
		}
	}
	
	return normalized;
}

Mat norm_image_with_max(Mat input, double &max)
{	
	max = 0;
	
	double min = 1000000;
	
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			if (input.at<double>(i, j) > max)
				max = input.at<double>(i, j);
			if (input.at<double>(i, j) < min)
				min = input.at<double>(i, j);
		}
	}
	
	Mat normalized(input.size(), CV_64FC1);

	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			normalized.at<double>(i, j) = ((input.at<double>(i, j) - min) / (max - min));
		}
	}
	
	return normalized;
}

Mat norm_image_char(Mat input)
{
	double max = 0;
	
	double min = 1000000;
	
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			if (input.at<double>(i, j) > max)
				max = input.at<double>(i, j);
			if (input.at<double>(i, j) < min)
				min = input.at<double>(i, j);
		}
	}

	Mat normalized(input.size(), CV_8UC1);
	cout << max << endl;
	cout << min << endl;
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			normalized.at<char>(i, j) = (char) (((input.at<double>(i, j) - min) / (max - min)) * 255);
		}
	}
	
	return normalized;
}

Mat norm_char_set_max(Mat input, double max)
{
	double min = 1000000;
	
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			if (input.at<double>(i, j) < min)
				min = input.at<double>(i, j);
		}
	}

	Mat normalized(input.size(), CV_8UC1);
	cout << max << endl;
	cout << min << endl;
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			normalized.at<char>(i, j) = (char) (((input.at<double>(i, j) - min) / (max - min)) * 255);
		}
	}
	
	return normalized;
}
	

/** @function detectAndDisplay */
vector<Mat> detectAndDisplay( Mat frame, string name, vector<Point2i> &eye_index, vector<Rect> &faces, vector<vector<Rect> > &eye_rects)
{
//	std::vector<Rect> faces;
	Mat frame_gray;
	Mat not_hist;
	cvtColor( frame, frame_gray, CV_BGR2GRAY );
	cvtColor(frame, not_hist, CV_BGR2GRAY);
	equalizeHist( frame_gray, frame_gray );
	
	//imshow("test gray", frame_gray);
	//waitKey(0);
	//-- Detect faces
	face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
	vector<Mat> eye_mats;
	for( size_t i = 0; i < faces.size(); i++ )
	{
		Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
		ellipse( frame, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );

		Mat faceROI = frame_gray( faces[i] );
		std::vector<Rect> eyes;

		//-- In each face, detect eyes
		eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );

		for( size_t j = 0; j < eyes.size(); j++ )
		{
			Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
			int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
			circle( frame, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
			Mat eye(eyes[j].width, eyes[j].height, CV_8U);
			cout << "Height: " << faces[i].y + eyes[j].y + eyes[j].height << endl;
			cout << "Width: " << faces[i].x + eyes[j].x + eyes[j].width << endl;
			cout << "Real W " << frame_gray.cols << endl;
			cout << "Real H " << frame_gray.rows << endl;
			char val = frame_gray.at<char>(100, 100);
			float real = float(val);
			cout << real << endl;

			
			for (int h = 0; h < eyes[j].height; h++) {
				for (int g= 0; g < eyes[j].width; g++) {
					eye.at<char>(h, g) = not_hist.at<char>(faces[i].y + eyes[j].y + h, faces[i].x + eyes[j].x + g);
				}
			}
			
			eye_mats.push_back(eye);
			eye_index.push_back(Point2i(faces[i].x + eyes[j].x, faces[i].y + eyes[j].y));
		}
		
		eye_rects.push_back(eyes);
	}

	
	return eye_mats;
	//-- Show what you got
	//imwrite("./output/" + name + ".jpg", frame);
	//imshow( window_name, frame );
}

const int STORAGE_SIZE = 1 << 12;

static void seqToMat(const CvSeq* seq, OutputArray _arr)
{
    if( seq && seq->total > 0 )
    {
        _arr.create(1, seq->total, seq->flags, -1, true);
        Mat arr = _arr.getMat();
        cvCvtSeqToArray(seq, arr.data);
    }
    else
        _arr.release();
}


static void
trevicvHoughCirclesGradient( CvMat* img, float dp, float min_dist,
                         int min_radius, int max_radius,
                         int canny_threshold, int acc_threshold,
                         CvSeq* circles, int circles_max )
{
    const int SHIFT = 10, ONE = 1 << SHIFT;
    cv::Ptr<CvMat> dx, dy;
    cv::Ptr<CvMat> edges, accum, dist_buf;
    std::vector<int> sort_buf;
    cv::Ptr<CvMemStorage> storage;

    int x, y, i, j, k, center_count, nz_count;
    float min_radius2 = (float)min_radius*min_radius;
    float max_radius2 = (float)max_radius*max_radius;
    int rows, cols, arows, acols;
    int astep, *adata;
    float* ddata;
    CvSeq *nz, *centers;
    float idp, dr;
    CvSeqReader reader;

    edges = cvCreateMat( img->rows, img->cols, CV_8UC1 );
    cvCanny( img, edges, MAX(canny_threshold/2,1), canny_threshold, 3 );

    dx = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    dy = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    cvSobel( img, dx, 1, 0, 3 );
    cvSobel( img, dy, 0, 1, 3 );

    if( dp < 1.f )
        dp = 1.f;
    idp = 1.f/dp;
    accum = cvCreateMat( cvCeil(img->rows*idp)+2, cvCeil(img->cols*idp)+2, CV_32SC1 );
    cvZero(accum);
	
    storage = cvCreateMemStorage();
    nz = cvCreateSeq( CV_32SC2, sizeof(CvSeq), sizeof(CvPoint), storage );
    centers = cvCreateSeq( CV_32SC1, sizeof(CvSeq), sizeof(int), storage );

    rows = img->rows;
    cols = img->cols;
    arows = accum->rows - 2;
    acols = accum->cols - 2;
    adata = accum->data.i;
    astep = accum->step/sizeof(adata[0]);
    // Accumulate circle evidence for each edge pixel
    for( y = 0; y < rows; y++ )
    {
        const uchar* edges_row = edges->data.ptr + y*edges->step;
        const short* dx_row = (const short*)(dx->data.ptr + y*dx->step);
        const short* dy_row = (const short*)(dy->data.ptr + y*dy->step);

        for( x = 0; x < cols; x++ )
        {
            float vx, vy;
            int sx, sy, x0, y0, x1, y1, r;
            CvPoint pt;

            vx = dx_row[x];
            vy = dy_row[x];

            if( !edges_row[x] || (vx == 0 && vy == 0) )
                continue;

            float mag = sqrt(vx*vx+vy*vy);
            assert( mag >= 1 );
            sx = cvRound((vx*idp)*ONE/mag);
            sy = cvRound((vy*idp)*ONE/mag);

            x0 = cvRound((x*idp)*ONE);
            y0 = cvRound((y*idp)*ONE);
            // Step from min_radius to max_radius in both directions of the gradient
            for(int k1 = 0; k1 < 2; k1++ )
            {
                x1 = x0 + min_radius * sx;
                y1 = y0 + min_radius * sy;

                for( r = min_radius; r <= max_radius; x1 += sx, y1 += sy, r++ )
                {
                    int x2 = x1 >> SHIFT, y2 = y1 >> SHIFT;
                    if( (unsigned)x2 >= (unsigned)acols ||
                        (unsigned)y2 >= (unsigned)arows )
                        break;
                    adata[y2*astep + x2]++;
                }

                sx = -sx; sy = -sy;
            }

            pt.x = x; pt.y = y;
            cvSeqPush( nz, &pt );
        }
    }
    
    int total_center_pts = nz->total;
    int highest = 0;
    Mat accumulator(accum, true);
    for (int i = 0; i < accumulator.rows; i++) {
		for (int j = 0; j < accumulator.cols; j++) {
			if (accumulator.at<int>(i, j) > highest)
				highest = accumulator.at<int>(i, j);
		}
	}
	if (accumulator.depth() == CV_8U)
		cout << "ITS UNSIGNED" << endl;
	else if (accumulator.depth() == CV_32F)
		cout << "ITS A FLOAT" << endl;
	else if (accumulator.depth() == CV_32S)
		cout << "SIGNED INT" << endl;
	Mat new_accumulator( accumulator.rows,  accumulator.cols, CV_8UC1);
	for (int i = 0; i < accumulator.rows; i++) {
		for (int j = 0; j < accumulator.cols; j++) {
			//new_accumulator.at<double>(i, j) = double(accumulator.at<int>(i, j)) / double(highest);
			new_accumulator.at<char>(i, j) = char(accumulator.at<int>(i, j));
		}
	}
	
	equalizeHist(new_accumulator, new_accumulator);
	cout << "displaying image!" << endl;
	cout << highest << endl;
	imwrite("./output/accumulator/george/eye0.jpg", new_accumulator);
	imshow("accumul ator", new_accumulator);
	
	waitKey(0);
/*
    nz_count = nz->total;
    if( !nz_count )
        return;
    //Find possible circle centers
    for( y = 1; y < arows - 1; y++ )
    {
        for( x = 1; x < acols - 1; x++ )
        {
            int base = y*(acols+2) + x;
            if( adata[base] > acc_threshold &&
                adata[base] > adata[base-1] && adata[base] > adata[base+1] &&
                adata[base] > adata[base-acols-2] && adata[base] > adata[base+acols+2] )
                cvSeqPush(centers, &base);
        }
    }

    center_count = centers->total;
    if( !center_count )
        return;

    sort_buf.resize( MAX(center_count,nz_count) );
    cvCvtSeqToArray( centers, &sort_buf[0] );

    icvHoughSortDescent32s( &sort_buf[0], center_count, adata );
    cvClearSeq( centers );
    cvSeqPushMulti( centers, &sort_buf[0], center_count );

    dist_buf = cvCreateMat( 1, nz_count, CV_32FC1 );
    ddata = dist_buf->data.fl;

    dr = dp;
    min_dist = MAX( min_dist, dp );
    min_dist *= min_dist;
    // For each found possible center
    // Estimate radius and check support
    for( i = 0; i < centers->total; i++ )
    {
        int ofs = *(int*)cvGetSeqElem( centers, i );
        y = ofs/(acols+2);
        x = ofs - (y)*(acols+2);
        //Calculate circle's center in pixels
        float cx = (float)((x + 0.5f)*dp), cy = (float)(( y + 0.5f )*dp);
        float start_dist, dist_sum;
        float r_best = 0;
        int max_count = 0;
        // Check distance with previously detected circles
        for( j = 0; j < circles->total; j++ )
        {
            float* c = (float*)cvGetSeqElem( circles, j );
            if( (c[0] - cx)*(c[0] - cx) + (c[1] - cy)*(c[1] - cy) < min_dist )
                break;
        }

        if( j < circles->total )
            continue;
        // Estimate best radius
        cvStartReadSeq( nz, &reader );
        for( j = k = 0; j < nz_count; j++ )
        {
            CvPoint pt;
            float _dx, _dy, _r2;
            CV_READ_SEQ_ELEM( pt, reader );
            _dx = cx - pt.x; _dy = cy - pt.y;
            _r2 = _dx*_dx + _dy*_dy;
            if(min_radius2 <= _r2 && _r2 <= max_radius2 )
            {
                ddata[k] = _r2;
                sort_buf[k] = k;
                k++;
            }
        }

        int nz_count1 = k, start_idx = nz_count1 - 1;
        if( nz_count1 == 0 )
            continue;
        dist_buf->cols = nz_count1;
        cvPow( dist_buf, dist_buf, 0.5 );
        icvHoughSortDescent32s( &sort_buf[0], nz_count1, (int*)ddata );

        dist_sum = start_dist = ddata[sort_buf[nz_count1-1]];
        for( j = nz_count1 - 2; j >= 0; j-- )
        {
            float d = ddata[sort_buf[j]];

            if( d > max_radius )
                break;

            if( d - start_dist > dr )
            {
                float r_cur = ddata[sort_buf[(j + start_idx)/2]];
                if( (start_idx - j)*r_best >= max_count*r_cur ||
                    (r_best < FLT_EPSILON && start_idx - j >= max_count) )
                {
                    r_best = r_cur;
                    max_count = start_idx - j;
                }
                start_dist = d;
                start_idx = j;
                dist_sum = 0;
            }
            dist_sum += d;
        }
        // Check if the circle has enough support
        if( max_count > acc_threshold )
        {
            float c[3];
            c[0] = cx;
            c[1] = cy;
            c[2] = (float)r_best;
            cvSeqPush( circles, c );
            if( circles->total > circles_max )
                return;
        }
    }*/
}

//CV_IMPL 
CvSeq* trevCvHoughCircles( CvArr* src_image, void* circle_storage,
                int method, double dp, double min_dist,
                double param1, double param2,
                int min_radius, int max_radius )
{
    CvSeq* result = 0;

    CvMat stub, *img = (CvMat*)src_image;
    CvMat* mat = 0;
    CvSeq* circles = 0;
    CvSeq circles_header;
    CvSeqBlock circles_block;
    int circles_max = INT_MAX;
    int canny_threshold = cvRound(param1);
    int acc_threshold = cvRound(param2);

    img = cvGetMat( img, &stub );

    if( !CV_IS_MASK_ARR(img))
        CV_Error( CV_StsBadArg, "The source image must be 8-bit, single-channel" );

    if( !circle_storage )
        CV_Error( CV_StsNullPtr, "NULL destination" );

    if( dp <= 0 || min_dist <= 0 || canny_threshold <= 0 || acc_threshold <= 0 )
        CV_Error( CV_StsOutOfRange, "dp, min_dist, canny_threshold and acc_threshold must be all positive numbers" );

    min_radius = MAX( min_radius, 0 );
    if( max_radius <= 0 )
        max_radius = MAX( img->rows, img->cols );
    else if( max_radius <= min_radius )
        max_radius = min_radius + 2;

    if( CV_IS_STORAGE( circle_storage ))
    {
        circles = cvCreateSeq( CV_32FC3, sizeof(CvSeq),
            sizeof(float)*3, (CvMemStorage*)circle_storage );
    }
    else if( CV_IS_MAT( circle_storage ))
    {
        mat = (CvMat*)circle_storage;

        if( !CV_IS_MAT_CONT( mat->type ) || (mat->rows != 1 && mat->cols != 1) ||
            CV_MAT_TYPE(mat->type) != CV_32FC3 )
            CV_Error( CV_StsBadArg,
            "The destination matrix should be continuous and have a single row or a single column" );

        circles = cvMakeSeqHeaderForArray( CV_32FC3, sizeof(CvSeq), sizeof(float)*3,
                mat->data.ptr, mat->rows + mat->cols - 1, &circles_header, &circles_block );
        circles_max = circles->total;
        cvClearSeq( circles );
    }
    else
        CV_Error( CV_StsBadArg, "Destination is not CvMemStorage* nor CvMat*" );

    switch( method )
    {
    case CV_HOUGH_GRADIENT:
        trevicvHoughCirclesGradient( img, (float)dp, (float)min_dist,
                                min_radius, max_radius, canny_threshold,
                                acc_threshold, circles, circles_max );
          break;
    default:
        CV_Error( CV_StsBadArg, "Unrecognized method id" );
    }

    if( mat )
    {
        if( mat->cols > mat->rows )
            mat->cols = circles->total;
        else
            mat->rows = circles->total;
    }
    else
        result = circles;

    //return result;
}

void trevHoughCircles( InputArray _image, OutputArray _circles,
                       int method, double dp, double min_dist,
                       double param1, double param2,
                       int minRadius, int maxRadius )
{
    Ptr<CvMemStorage> storage = cvCreateMemStorage(STORAGE_SIZE);
    Mat image = _image.getMat();
    CvMat c_image = image;
    //CvSeq* seq = cvHoughCircles( &c_image, storage, method,
      //              dp, min_dist, param1, param2, minRadius, maxRadius );
    CvSeq* seq = trevCvHoughCircles( &c_image, storage, method,
                    dp, min_dist, param1, param2, minRadius, maxRadius );
    CvSeq *test;
   // seqToMat(seq, _circles);
}


