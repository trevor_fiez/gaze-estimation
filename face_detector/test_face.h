#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/imgproc/imgproc_c.h>
//#include <opencv2/highgui/highgui_c.hpp>
#include <ctype.h>
//#include "/home/trevor/opencv-2.4.9/modules/imgproc/include/opencv2/imgproc/imgproc_c.h"
//#include "/home/trevor/opencv-2.4.9/modules/imgproc/include/opencv2/imgproc/types_c.h"
//#include "/home/trevor/opencv-2.4.9/modules/core/include/opencv2/core/core_c.hpp"
//#include "precomp.hpp"
#include "_list.h"

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <queue>

using namespace std;
using namespace cv;


class gx_pt {
	public:
		gx_pt (int pos_x, int pos_y, Point2d p, double gx_mag);
	public:
		int x;
		int y;
		Point2d phase;
		double mag;
};

class eyes_data {
	public:
		eyes_data (string name);
		eyes_data(string new_name, vector<Mat> eyes, Rect face_rect, vector<Rect> eye_rectangle, vector<Point2i> eye_offsets);
		void create_eye_vid(string eye_vid_name, vector<vector<Mat> > &frames);
		vector<Point2i> pupil_locations(vector<Mat> g_xs, vector<Mat> g_ys, vector<Mat> g_mags, vector<Mat> eyes);
		void create_phase_vectors ();
		void create_filtered_gx(double thresh);
		void compute_gradients();
		void gx_list (double threshold);
		void create_distance_mat();
		double proposed_loc_score(int p_y, int p_x, vector<gx_pt> &pts, double iris_size=23);
		void create_pupil_scores_all();
		void wrapper();
		void median_blur_eyes(int size);
		void draw_face();
		void update_cur_frame(Mat new_frame);
		void all_scores_highest();
		void local_search(int prev_x, int prev_y, vector<gx_pt> &pt_list, Mat eye, int num_limit, double iris_size=23);
		void local_wrapper();
		Mat create_eye_quilt(int eye_num);
		void search_features();
		void compute_both_searches ();
		void average_loc_filter(int look_size);
		void set_gx_threshold();
		void kmeans_filter(int look_size);
		void set_iris_size();
		double create_iris_scores(int eye, int iris_size);
		void kmeans_velocity_filter(int look_size);
		void gx_list_gradient_theshold ();
		void search_features_ref_thresh();
		void local_wrapper_ref_thresh();
		void local_search_ref(int prev_x, int prev_y, vector<gx_pt> &pt_list, Mat eye, int num_limit, double iris_s, int l_r);
	public:
		string name;
		vector<vector<Mat> > gx;
		vector<vector<Mat> > gy;
		vector<vector<Mat> > gmag;
		vector<vector<Mat> > pupil_loc_scores;
		vector<vector<Point2d> > cur_left_phase;
		vector<vector<Point2d> > cur_right_phase;
		vector<Mat> cur_gx;
		vector<Mat> reference_gx;
		vector<Mat> cur_gy;
		vector<Mat> cur_mag;
		vector<Mat> cur_eyes;
		vector<gx_pt> left_pts;
		vector<gx_pt> right_pts;
		Mat x_dis;
		Mat y_dis;
		Mat total_dis;
		vector<Mat> cur_scores;
		vector<Mat> cur_local_scores;
		Rect face;
		vector<Rect> eye_rects;
		vector<Point2i> eye_index;
		Mat cur_frame;
		VideoWriter pupil_locations_out;
		vector<Point2i> pupils;
		vector<Point2i> prev_pupils;
		vector<Point2i> abs_pupils;
		vector<double> loc_scores;
		vector<double> prev_loc_scores;
		vector<vector<Point2i> > prev_p_detections;
		vector<vector<Point2i> > pt_shown;
		double l_max_gx;
		double r_max_gx;
		double cur_l_gx;
		double cur_r_gx;
		double threshold_const;
		int consec_blinks;
		int largest_blinks;
		vector<vector<Mat> > iris_mats;
		vector<double> iris_size;
		vector<vector<double> >iris_max_score;
		vector<Point2i> ref_pt;
};


vector<Mat> detectAndDisplay( Mat frame, string name, vector<Point2i> &eye_index, vector<Rect> &faces, vector<vector<Rect> > &eye_rects);
void trevHoughCircles( InputArray _image, OutputArray _circles,
                       int method, double dp, double min_dist,
                       double param1, double param2,
                       int minRadius, int maxRadius );
                       
Mat smooth_image(Mat input, int ksize, double sigma);
Mat compute_x_gradient(Mat input);
Mat norm_image(Mat input);
Mat compute_y_gradient(Mat input);
Mat compute_g_mag(Mat gx, Mat gy);
Mat compute_g_phase(Mat gx, Mat gy);

void norm_phase_vector(vector<vector<Point2d> > &phase);
Mat pupil_scores(vector<vector<Point2d> > phase, Mat image, Mat g_mag);
vector<vector<Point2d> > phase_vectors(Mat gx, Mat gy, Mat g_mag);
void left_right_eyes(vector<Mat> &eyes, vector<Point2i> &eye_locs, vector<vector<Rect> > eye_rects);
Mat threshold_mag(Mat mag, double threshold);

void face_eye_detection_filtering(vector<Point2i> &eyes, vector<Mat> &eye_mat, vector<Rect> &faces, Point2i im_size, vector<vector<Rect> > &eye_rects);
void draw_face(Mat &frame, vector<Rect> faces, vector<Rect> eyes);
int alt_main (int argc, char *argv[]);
vector<Point2i> pupil_locations(vector<Mat> g_xs, vector<Mat> g_ys, vector<Mat> g_mags, vector<Mat> eyes, eyes_data &data);
Mat test_pupil_scores(vector<vector<Point2d> > phase, Mat image, Mat g_mag, int scan_window);

Mat abs_threshold_mag(Mat mag, double threshold);
Mat norm_image_char(Mat input);
double dot(Point2d a, Point2d b);
double gauss_dis(double dis);
Mat norm_image_with_max(Mat input, double &max);
double gauss_set_dis(double dis, double offset);
Mat norm_char_set_max(Mat input, double max);

